#!/bin/bash
set -e

alias vim="nvim"
alias emacs="doom"

if [[ ${XDG_CURRENT_DESKTOP} == 'ubuntu:GNOME' ]]; then
    gnome-control-center appearance
fi

echo 'Install installing things in apt'
sudo apt update && sudo apt upgrade -y
sudo apt install -y curl open-vm-tools sdkmanager git ripgrep zsh fish tmux python3-pip thefuck awscli podman kubecolor mosh tree fuse3 mosh ca-certificates dconf-cli uuid-runtime xstow pass meson cmake automake autoconf make xclip w3m pylint python3-flake8 libtool gconf2

echo 'Adding apt sources'
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/focal.noarmor.gpg | sudo tee /usr/share/keyrings/tailscale-archive-keyring.gpg >/dev/null
curl -fsSL https://pkgs.tailscale.com/stable/ubuntu/focal.tailscale-keyring.list | sudo tee /etc/apt/sources.list.d/tailscale.list
curl -fsSL https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg >/dev/null
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo tee /etc/apt/keyrings/kubernetes-archive-keyring.gpg >/dev/null
echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

echo 'Updating apt and installing more'
sudo apt update 
sudo apt -y install vagrant terraform kubectl tailscale
sudo apt -q remove vim

echo 'Installing things in snap'
snap_packages="bitwarden bw searchsploit docker sqlmap-cli spotify zoom-client slack slack-term"
for package in $(echo $snap_packages); do
    sudo snap install $package
done
sudo snap install helm --classic 
sudo snap install emacs --classic 
sudo snap install microk8s --classic 
sudo snap install task --classic 
sudo snap install go --classic 
sudo snap install nvim --classic 
sudo snap install tugboat --beta

echo 'Installing things in pip'
pip3 install autoenv pipenv

echo 'Importing GPG publics'
curl -L https://keybase.io/bkoch53/pgp_keys.asc | gpg --import
echo -e "5\ny\n" | gpg --command-fd 0 --edit-key 0CCBFDE020A8F3A379F1353FE31F843EB7A78A5F trust

echo 'Making directories for stuff'
mkdir -p ~/{.fonts,source}

echo 'Downloading fonts'
pushd ~/.fonts
wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf'
wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf'
wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf'
wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf'
sudo mkdir -p /usr/share/fonts/truetype/MesloLGS/
sudo cp ./*.ttf /usr/share/fonts/truetype/MesloLGS/
fc-cache -fv
popd

echo 'Installing oh-my-zsh'
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sudo usermod -s /usr/bin/zsh enigma
newgrp $USER

echo 'Installing oh-my-tmux'
git clone https://github.com/gpakosz/.tmux.git
ln -s -f .tmux/.tmux.conf

echo 'Installing powerlevel10k'
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/.oh-my-zsh/custom/themes/powerlevel10k

echo 'Installing GOGH colors'
# bash -c '$(wget -qO- https://git.io/vQgMr)'
read -p "You need to create a new profile in your terminal" meow
pushd ~/source
git clone https://github.com/Mayccoll/Gogh.git gogh
export TERMINAL=gnome-terminal
pushd gogh/install
./dracula.sh
popd;popd

echo 'Installing doomemacs'
echo "PATH=$PATH:~/.config/emacs/bin"
git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.config/emacs
export PATH=$PATH:~/.config/emacs/bin
~/.config/emacs/bin/doom install --force

pushd ~/source
echo 'Installing Vivaldi'
wget -q -O ./vivaldi.deb 'https://downloads.vivaldi.com/stable/vivaldi-stable_5.7.2921.68-1_amd64.deb'
sudo dpkg -i ./vivaldi.deb
rm ./vivaldi.dev
popd

echo 'Installing python poetry'
curl -sSL https://install.python-poetry.org | python3 -

echo 'Installing please build system'
curl https://get.please.build | bash

echo 'Installing kickstart nvim'
mkdir -p ~/.config/nvim &>/dev/null
pushd ~/.config/nvim
git clone https://github.com/nvim-lua/kickstart.nvim.git .
nvim
popd

echo 'Creating new SSH key'
ssh-keygen -q -t rsa -b 4096 -f ~/.ssh/$(hostname)-${USER} -N '' <<<y

echo 'Pulling configs from personal git'
git clone https://gitlab.com/bkoch53/dotfiles ~/.dots
pushd ~/.dots
git config user.email enigmauser@protonmail.com
git config remote.origin.url git@gitlab.com:bkoch53/dotfiles
popd
rm ~/.zshrc ~/.config/doom/* ~/.gitconfig ~/.gnupg/gpg-agent.conf ~/.bashrc
xstow -d ~/.dots zsh doom bin bash dev-setup tmux git gpg p10k

echo "Syning doom now that we have the config"
doom sync

echo 'Starting tailscale'
sudo tailscale up

zsh
