# .Dots!
This repo contains configuration files and other useful related items.
![scrot](https://gitlab.com/bkoch53/dotfiles/raw/master/scrot.png)

## How to use this repo
Everything in here is designed to be installed via `GNU Stow`

```bash
curl -s https://gitlab.com/bkoch53/dotfiles/-/raw/master/dev-setup/dev-setup.sh | bash
wget -q -O- https://gitlab.com/bkoch53/dotfiles/-/raw/master/dev-setup/dev-setup.sh | bash
```
```bash
sudo apt install xstow
git clone git@gitlab.com:bkoch53/dotfiles ~/.dots
pushd ~/.dots
stow i3
stow polybar
ls -la ~/.config/i3/
popd
```


## Packages
* stow
* pass
* i3-gaps
* polybar
* wal
* https://github.com/daniruiz/Flat-Remix-GTK
