(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("ff24d14f5f7d355f47d53fd016565ed128bf3af30eb7ce8cae307ee4fe7f3fd0" "ea92c5b0b47f1c236c61b5ccd68685bf2585a75ccee72940f3614f912e91d3b6" default))
 '(elfeed-feeds
   '(("https://www.darkreading.com/rss_simple.asp")
     ("https://krebsonsecurity.com/feed/")
     ("https://feeds.feedburner.com/TheHackersNews")
     ("https://www.securityweek.com/rss.xml")
     ("https://threatpost.com/feed/")
     ("https://nakedsecurity.sophos.com/feed/")
     ("https://www.infosecurity-magazine.com/rss/news")
     ("https://www.scmagazine.com/feed/")
     ("https://www.csoonline.com/index.rss")
     ("https://securityaffairs.co/wordpress/feed")
     ("https://isc.sans.edu/rssfeed.xml")
     ("https://insights.sei.cmu.edu/cert/atom.xml")))
 '(package-selected-packages '(catppuccin-theme autothemer))
 '(warning-suppress-log-types '((initialization) (initialization))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
