static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#eec6c5", "#0e1010" },
	[SchemeSel] = { "#eec6c5", "#C83E3A" },
	[SchemeOut] = { "#eec6c5", "#E7817E" },
};
