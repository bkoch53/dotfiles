static const char norm_fg[] = "#eec6c5";
static const char norm_bg[] = "#0e1010";
static const char norm_border[] = "#a68a89";

static const char sel_fg[] = "#eec6c5";
static const char sel_bg[] = "#D7413D";
static const char sel_border[] = "#eec6c5";

static const char urg_fg[] = "#eec6c5";
static const char urg_bg[] = "#C83E3A";
static const char urg_border[] = "#C83E3A";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
