const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#0e1010", /* black   */
  [1] = "#C83E3A", /* red     */
  [2] = "#D7413D", /* green   */
  [3] = "#E2433F", /* yellow  */
  [4] = "#9D6867", /* blue    */
  [5] = "#E74D49", /* magenta */
  [6] = "#E7817E", /* cyan    */
  [7] = "#eec6c5", /* white   */

  /* 8 bright colors */
  [8]  = "#a68a89",  /* black   */
  [9]  = "#C83E3A",  /* red     */
  [10] = "#D7413D", /* green   */
  [11] = "#E2433F", /* yellow  */
  [12] = "#9D6867", /* blue    */
  [13] = "#E74D49", /* magenta */
  [14] = "#E7817E", /* cyan    */
  [15] = "#eec6c5", /* white   */

  /* special colors */
  [256] = "#0e1010", /* background */
  [257] = "#eec6c5", /* foreground */
  [258] = "#eec6c5",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
