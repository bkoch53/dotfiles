version: '3'

tasks:
  # --------------------------------------------------------------------------
  # Main "all" task
  # --------------------------------------------------------------------------
  all:
    desc: "Run all provisioning steps in order"
    deps:
      - install-nix
      - install-nix-packages
      - install-openrgb-udev
      - set-aliases-gnome
      - import-gpg
      - make-directories
      - download-fonts
      - oh-my-zsh
      - oh-my-tmux
      - powerlevel10k
      - gogh-colors
      - doom-emacs
      - poetry
      - please-build
      - nvim-kickstart
      - generate-ssh-key
      - pull-dotfiles
      - doom-sync
      - tailscale-up

  # --------------------------------------------------------------------------
  # 0. Install Nix
  # --------------------------------------------------------------------------
  install-nix:
    desc: "Install Nix if not present (multi-user), then source environment"
    cmds:
      - |
        if ! command -v nix &>/dev/null; then
          echo "Nix not found. Installing..."
          sh <(curl -L https://nixos.org/nix/install) --daemon
          echo "Nix installed successfully."

          # Source the Nix environment in this shell
          if [ -f "$HOME/.nix-profile/etc/profile.d/nix.sh" ]; then
            . "$HOME/.nix-profile/etc/profile.d/nix.sh"
          fi
        else
          echo "Nix already installed."
        fi

  # --------------------------------------------------------------------------
  # 0.1 Install Everything from nixpkgs
  # --------------------------------------------------------------------------
  install-nix-packages:
    desc: "Install packages using nix profile install (replaces apt/snap)"
    cmds:
      - for:
          matrix:
            PACKAGE:
              - nixpkgs#git
              - nixpkgs#ripgrep
              - nixpkgs#zsh
              - nixpkgs#fish
              - nixpkgs#tmux
              - nixpkgs#python3
              - nixpkgs#thefuck
              - nixpkgs#awscli
              - nixpkgs#podman
              - nixpkgs#kubecolor
              - nixpkgs#mosh
              - nixpkgs#fuse3
              - nixpkgs#xstow
              - nixpkgs#pass
              - nixpkgs#meson
              - nixpkgs#cmake
              - nixpkgs#automake
              - nixpkgs#autoconf
              - nixpkgs#gnumake
              - nixpkgs#xclip
              - nixpkgs#w3m
              - nixpkgs#libtool
              - nixpkgs#docker
              - nixpkgs#sqlmap
              - nixpkgs#spotify
              - nixpkgs#zoom-us
              - nixpkgs#slack
              - nixpkgs#terraform
              - nixpkgs#vagrant
              - nixpkgs#kubectl
              - nixpkgs#helm
              - nixpkgs#k9s
              - nixpkgs#go
              - nixpkgs#neovim
              - nixpkgs#vivaldi
              - nixpkgs#tailscale
              - nixpkgs#gcc
              - nixpkgs#openrgb
        cmd: NIXPKGS_ALLOW_UNFREE=1 nix profile install --impure "{{.ITEM.PACKAGE}}"

  # --------------------------------------------------------------------------
  # OpenRBG udev
  # --------------------------------------------------------------------------
  install-openrgb-udev:
    desc: "OpenRBG requires udev rules to be installed"
    cmds:
      - curl https://openrgb.org/releases/release_0.9/openrgb-udev-install.sh | sh

  # --------------------------------------------------------------------------
  # Set Aliases / GNOME Appearance
  # --------------------------------------------------------------------------
  set-aliases-gnome:
    desc: "Set some shell aliases; open GNOME appearance if ubuntu:GNOME"
    cmds:
      - |
        echo "Alias vim -> nvim, emacs -> doom (in this shell session)"
        alias vim="nvim"
        alias emacs="doom"

        if [[ "$XDG_CURRENT_DESKTOP" == 'ubuntu:GNOME' ]]; then
          echo "Detected ubuntu:GNOME; opening gnome-control-center appearance..."
          gnome-control-center appearance || true
        fi

  # --------------------------------------------------------------------------
  # Import GPG Key
  # --------------------------------------------------------------------------
  import-gpg:
    desc: "Import GPG public key from keybase"
    cmds:
      - |
        echo "Importing GPG key..."
        curl -L https://keybase.io/bkoch53/pgp_keys.asc | gpg --import
        # Setting trust:
        echo -e "5\ny\n" | gpg --command-fd 0 --edit-key 0CCBFDE020A8F3A379F1353FE31F843EB7A78A5F trust

  # --------------------------------------------------------------------------
  # Make Directories
  # --------------------------------------------------------------------------
  make-directories:
    desc: "Create .fonts and source directories"
    cmds:
      - |
        mkdir -p ~/.fonts
        mkdir -p ~/source

  # --------------------------------------------------------------------------
  # Download Fonts
  # --------------------------------------------------------------------------
  download-fonts:
    desc: "Install MesloLGS NF fonts for powerlevel10k"
    cmds:
      - |
        echo "Downloading MesloLGS NF fonts..."
        pushd ~/.fonts
        wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf'
        wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf'
        wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf'
        wget 'https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf'
        sudo mkdir -p /usr/share/fonts/truetype/MesloLGS/
        sudo cp ./*.ttf /usr/share/fonts/truetype/MesloLGS/
        fc-cache -fv
        popd

  # --------------------------------------------------------------------------
  # Oh-My-Zsh
  # --------------------------------------------------------------------------
  oh-my-zsh:
    desc: "Install oh-my-zsh"
    cmds:
      - |
        echo "Installing oh-my-zsh..."
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
          || true
        # Switch default shell for user 'enigma' (adjust as needed)
        sudo usermod -s /usr/bin/zsh enigma || true
        newgrp "$USER" || true

  # --------------------------------------------------------------------------
  # Oh-My-Tmux
  # --------------------------------------------------------------------------
  oh-my-tmux:
    desc: "Clone oh-my-tmux and symlink tmux.conf"
    cmds:
      - |
        git clone https://github.com/gpakosz/.tmux.git ~/.tmux || true
        ln -s -f ~/.tmux/.tmux.conf ~

  # --------------------------------------------------------------------------
  # Powerlevel10k
  # --------------------------------------------------------------------------
  powerlevel10k:
    desc: "Install powerlevel10k theme for oh-my-zsh"
    cmds:
      - |
        echo "Installing p10k..."
        git clone --depth=1 https://github.com/romkatv/powerlevel10k.git \
          ~/.oh-my-zsh/custom/themes/powerlevel10k || true

  # --------------------------------------------------------------------------
  # Gogh Colors
  # --------------------------------------------------------------------------
  gogh-colors:
    desc: "Install Dracula theme from Gogh"
    cmds:
      - |
        echo "You may need a new terminal profile. Press Enter to continue..."
        read -r
        pushd ~/source
        git clone https://github.com/Mayccoll/Gogh.git gogh || true
        export TERMINAL=gnome-terminal
        pushd gogh/install
        ./dracula.sh || true
        popd
        popd

  # --------------------------------------------------------------------------
  # Doom Emacs
  # --------------------------------------------------------------------------
  doom-emacs:
    desc: "Install Doom Emacs in ~/.config/emacs"
    cmds:
      - |
        echo "Installing Doom Emacs..."
        git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.config/emacs || true
        export PATH="$PATH:~/.config/emacs/bin"
        ~/.config/emacs/bin/doom install --force

  # --------------------------------------------------------------------------
  # Poetry
  # --------------------------------------------------------------------------
  poetry:
    desc: "Install python poetry (if you prefer Nix, comment this out)"
    cmds:
      - |
        echo "Installing poetry directly from the official script..."
        curl -sSL https://install.python-poetry.org | python3 -

  # --------------------------------------------------------------------------
  # Please Build System
  # --------------------------------------------------------------------------
  please-build:
    desc: "Install Please build system (again, optional if you prefer Nix version)"
    cmds:
      - |
        echo "Installing please..."
        curl https://get.please.build | bash

  # --------------------------------------------------------------------------
  # Neovim Kickstart
  # --------------------------------------------------------------------------
  nvim-kickstart:
    desc: "Bootstrap Neovim config using kickstart.nvim"
    cmds:
      - |
        mkdir -p ~/.config/nvim
        pushd ~/.config/nvim
        git clone https://github.com/nvim-lua/kickstart.nvim.git . || true
        nvim || true  # open Neovim once to initialize
        popd

  # --------------------------------------------------------------------------
  # Generate SSH Key
  # --------------------------------------------------------------------------
  generate-ssh-key:
    desc: "Create a new SSH key"
    cmds:
      - |
        echo "Generating SSH key..."
        ssh-keygen -q -t rsa -b 4096 -f ~/.ssh/$(hostname)-${USER} -N '' <<<y

  # --------------------------------------------------------------------------
  # Pull Dotfiles & Stow
  # --------------------------------------------------------------------------
  pull-dotfiles:
    desc: "Clone personal dotfiles and stow them"
    cmds:
      - |
        echo "Cloning personal dotfiles..."
        git clone https://gitlab.com/bkoch53/dotfiles ~/.dots || true
        pushd ~/.dots
        git config user.email "enigmauser@protonmail.com"
        git config remote.origin.url git@gitlab.com:bkoch53/dotfiles
        popd
        # Remove defaults to avoid conflicts
        rm -f ~/.zshrc ~/.config/doom/* ~/.gitconfig ~/.gnupg/gpg-agent.conf ~/.bashrc
        echo "Stowing dotfiles..."
        xstow -d ~/.dots zsh doom bin bash dev-setup tmux git gpg p10k

  # --------------------------------------------------------------------------
  # Doom Sync
  # --------------------------------------------------------------------------
  doom-sync:
    desc: "Sync Doom Emacs config after stowing"
    cmds:
      - |
        doom sync || ~/.config/emacs/bin/doom sync || true

  # --------------------------------------------------------------------------
  # Tailscale Up
  # --------------------------------------------------------------------------
  tailscale-up:
    desc: "Start Tailscale (installed from Nix above)"
    cmds:
      - |
        sudo tailscale up || true
