call plug#begin('~/.vim/plugdir')

Plug 'https://github.com/dylanaraps/wal.vim'

call plug#end()

set number
set tabstop=4
set shiftwidth=4
set expandtab
set paste
set cursorline
set colorcolumn=85


autocmd! User wal.vim

colorscheme wal
" source ~/.vim/colors/molokai.vim
