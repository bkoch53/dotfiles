#!/usr/bin/env bash
set -euo pipefail

# Install Task if missing:
if ! command -v task &>/dev/null; then
  echo "Task not found. Installing..."
  sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d
fi

# Now run all tasks:
task all
